// baitap1
// input: - lương nhân viên 
//        (Lương 1 ngày: 100.000)
//        - Số ngày làm
//  Step: - Tạo 2 biến lưu input
//        - Tạo 1 biến lưu Output
//        - Tính theo công thức: Lương x Ngày làm
// 
//  Output: - Luong nhân viên
function luong() {
    var a = 100000;
    var b = Number(document.getElementById("day").value);

    var luong = a * b;

    document.getElementById("result").innerHTML = luong;
}


// baitap2
// input: - 5 số thực
// 
//  Step: - Tạo 5 biến lưu input
//        - Tạo 1 biến lưu Output
//        - Tính theo công thức: giá trị trung bình của 5 số chia cho 5
// Output: - 3
function gttb() {
    var n1 = Number(document.getElementById("num1").value);
    var n2 = Number(document.getElementById("num2").value);
    var n3 = Number(document.getElementById("num3").value);
    var n4 = Number(document.getElementById("num4").value);
    var n5 = Number(document.getElementById("num5").value);

    var gttb = (n1 + n2 + n3 + n4 + n5) / 5;

    document.getElementById("result1").innerHTML = gttb;
}


// baitap3
// input: - 1USD= 23.500VND
//        - Số tiền cần đổi sang VND
//  Step: - Tạo 2 biến lưu input
//        - Tạo 1 biến lưu Output
//        - Tính theo công thức: số tiền USD cần đổi x 23.500VND
// Output:- So tien da chuyen doi
function chuyendoi() {
    var m1 = 23500;
    var m2 = Number(document.getElementById("money").value);

    var chuyendoi = m1 * m2;

    document.getElementById("result2").innerHTML = chuyendoi;
}


// baitap4
//input: - Chieu dai HCN
//        - Chieu rong HCN
// 
//  Step: - Tạo 2 biến lưu input
//        - Tạo 2 biến lưu Output
//        - Tính theo công thức: + Dien tich HCN: Chieu dai x Chieu rong
//                               + Chu vi HCN: (Chieu dai x Chieu rong) x 2
// Output:- Dien tich HCN
//        - Chu vi HCN

function tinh() {
    var d = Number(document.getElementById("dai").value);
    var r = Number(document.getElementById("rong").value);

    var chuvi = (d + r) * 2;
    var dientich = d * r;

    document.getElementById("result3").innerHTML = chuvi;
    document.getElementById("result4").innerHTML = dientich;
}


// baitap5
// input: - Nhap vao 1 so có 2 chữ số
//      
//  Step: - Tạo 3 biến lưu input
//        - Tạo 1 biến lưu Output
//        - Tính theo công thức: + Lấy hàng đơn vị:  chia 10 lấy phần dư
//                               + Lấy hàng chục: chia 10 lấy phần nguyên
// Output:- Tổng 2 số
function total() {
    var number1 = Number(document.getElementById("number").value);

    var number2 = parseInt(number1) % 10;
    var number3 = parseInt(number1) / 10;

    var total = parseInt(number2) + parseInt(number3);

    document.getElementById("result5").innerHTML = total;
}
